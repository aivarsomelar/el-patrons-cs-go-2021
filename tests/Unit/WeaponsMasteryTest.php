<?php

namespace Tests\Unit;

use App\Mastery\WeaponsMastery;
use PHPUnit\Framework\TestCase;

class WeaponsMasteryTest extends TestCase
{
    /**
     * @dataProvider playersData
     * @param $playersData
     */
    public function testGetWeaponMasters($playersData)
    {

        $weaponMastery = new WeaponsMastery();
        $results = $weaponMastery->getWeaponMasters($playersData);
        $this->assertArrayHasKey('76561198441550778', $results);
        foreach ($results as $player) {
            $this->assertArrayHasKey('mastery', $player);
            $this->assertArrayHasKey('pistol', $player['mastery']);
        }
    }

    /**
     * @return array
     */
    public function playersData(): array
    {

        return [
            [
                [
                    '76561198441550778' => [
                        'stats' => [
                            'total_kills' => 29748,
                            "total_kills_glock" => 668,
                            'total_kills_deagle' => 1667,
                            'total_kills_elite' => 272,
                            'total_kills_fiveseven' => 37,
                            'total_kills_xm1014' => 222,
                            'total_kills_mac10' => 423,
                            'total_kills_ump45' => 211,
                            'total_kills_p90' => 368,
                            'total_kills_awp' => 520,
                            'total_kills_ak47' => 10701,
                            'total_kills_aug' => 340,
                            'total_kills_famas' => 207,
                            'total_kills_g3sg1' => 183,
                            'total_kills_m249' => 131,
                            'total_kills_hkp2000' => 1062,
                            'total_kills_p250' => 1365,
                            'total_kills_sg556' => 427,
                            'total_kills_scar20' => 158,
                            'total_kills_ssg08' => 326,
                            'total_kills_mp7' => 505,
                            'total_kills_mp9' => 665,
                            'total_kills_nova' => 185,
                            'total_kills_negev' => 246,
                            'total_kills_sawedoff' => 107,
                            'total_kills_bizon' => 207,
                            'total_kills_tec9' => 74,
                            'total_kills_mag7' => 130,
                            'total_kills_m4a1' => 7531,
                            'total_kills_galilar' => 624
                        ]
                    ],
                    '76561198990574991' => [
                        'stats' => [
                            'total_kills' => 20748,
                            "total_kills_glock" => 668,
                            'total_kills_deagle' => 1667,
                            'total_kills_elite' => 272,
                            'total_kills_fiveseven' => 37,
                            'total_kills_xm1014' => 222,
                            'total_kills_mac10' => 423,
                            'total_kills_ump45' => 211,
                            'total_kills_p90' => 368,
                            'total_kills_awp' => 520,
                            'total_kills_ak47' => 10701,
                            'total_kills_aug' => 340,
                            'total_kills_famas' => 207,
                            'total_kills_g3sg1' => 183,
                            'total_kills_m249' => 131,
                            'total_kills_hkp2000' => 1062,
                            'total_kills_p250' => 1365,
                            'total_kills_sg556' => 427,
                            'total_kills_scar20' => 158,
                            'total_kills_ssg08' => 326,
                            'total_kills_mp7' => 505,
                            'total_kills_mp9' => 665,
                            'total_kills_nova' => 185,
                            'total_kills_negev' => 246,
                            'total_kills_sawedoff' => 107,
                            'total_kills_bizon' => 207,
                            'total_kills_tec9' => 74,
                            'total_kills_mag7' => 130,
                            'total_kills_m4a1' => 7531,
                            'total_kills_galilar' => 624
                        ]
                    ],
                    '76561198362624411' => [
                        'stats' => [
                            'total_kills' => 30748,
                            "total_kills_glock" => 668,
                            'total_kills_deagle' => 1667,
                            'total_kills_elite' => 272,
                            'total_kills_fiveseven' => 37,
                            'total_kills_xm1014' => 222,
                            'total_kills_mac10' => 423,
                            'total_kills_ump45' => 211,
                            'total_kills_p90' => 368,
                            'total_kills_awp' => 520,
                            'total_kills_ak47' => 10701,
                            'total_kills_aug' => 340,
                            'total_kills_famas' => 207,
                            'total_kills_g3sg1' => 183,
                            'total_kills_m249' => 131,
                            'total_kills_hkp2000' => 1062,
                            'total_kills_p250' => 1365,
                            'total_kills_sg556' => 427,
                            'total_kills_scar20' => 158,
                            'total_kills_ssg08' => 326,
                            'total_kills_mp7' => 505,
                            'total_kills_mp9' => 665,
                            'total_kills_nova' => 185,
                            'total_kills_negev' => 246,
                            'total_kills_sawedoff' => 107,
                            'total_kills_bizon' => 207,
                            'total_kills_tec9' => 74,
                            'total_kills_mag7' => 130,
                            'total_kills_m4a1' => 7531,
                            'total_kills_galilar' => 624
                        ]
                    ]
                ]
            ]
        ];
    }
}
