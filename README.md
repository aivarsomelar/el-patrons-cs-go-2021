#ElPatrons SC:GO Stats

### How to start bot and leave it running in background
#### Method 1
```
php artisan discord:start
```
Press Ctrl+Z
```
bg
```

#### Method 2
```
php artisan discord:start . &
```

#### Check jobs running background
```
jobs
```
#### Stop bot
```
ps aux
```
```
kill {id}
```


