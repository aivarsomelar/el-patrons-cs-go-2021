<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ScoreBoardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', [ScoreBoardController::class, 'index']);
Route::get('/debug', [ScoreBoardController::class, 'debug']);
Route::get('test', [\App\Http\Controllers\MyTestController::class, 'steam']);

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::post('/addPlayer', [DashboardController::class, 'addPlayer'])->name('addPlayer');
    Route::post('/deletePlayer', [DashboardController::class, 'deletePlayer'])->name('deletePlayer');
});

require __DIR__.'/auth.php';
