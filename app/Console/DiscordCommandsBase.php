<?php

namespace App\Console;

use Discord\Discord;
use Illuminate\Console\Command;

/**
 * Class Discord Commands Base class
 * @package App\Console
 */
class DiscordCommandsBase extends Command
{

    /**
     * @var Discord
     */
    protected $discord;
}
