<?php

namespace App\Console\Commands;

use App\Console\DiscordCommandsBase;
use App\Http\Controllers\DiscordController;

class StartDiscord extends DiscordCommandsBase
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'discord:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start Discord bot';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->discord = (new DiscordController())->getDiscordInstance();

        echo "Starting discord bot \n";

        $this->discord->run();
    }
}
