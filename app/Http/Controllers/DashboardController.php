<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddPlayer;
use App\Http\Requests\DeletePlayer;
use App\Models\Player;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class DashboardController extends Controller
{

    /**
     * @return Application|Factory|View
     */
    public function index()
    {

        return view('dashboard')->with(['players' => Player::all()]);
    }

    /**
     * @param AddPlayer $addPlayer
     * @return RedirectResponse
     */
    public function addPlayer(AddPlayer $addPlayer)
    {

        (new Player(['name' => $addPlayer->name, 'steamId' => $addPlayer->steamId]))->save();

        return back();
    }

    /**
     * @param DeletePlayer $deletePlayer
     * @return string
     */
    public function deletePlayer(DeletePlayer $deletePlayer)
    {

       $player = Player::find($deletePlayer->id);
        $player->delete();

        return back();
    }
}
