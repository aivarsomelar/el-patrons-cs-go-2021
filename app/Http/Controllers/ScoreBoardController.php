<?php

namespace App\Http\Controllers;

use App\Mastery\WeaponsMastery;
use App\Models\Player;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Syntax\SteamApi\Steam\App as Steam;
use Illuminate\Database\Eloquent\Collection;

class ScoreBoardController extends Controller
{
    /**
     * @var array
     */
    const QUOTES = [
        'Mida v***u, See vend saab reporti',
        'Sometimes lose, always wins',
        'NOOBEZ',
        'Pask mäng, uninstall',
        'John Wikibong is son of a *****, which sucking big ***** everyday'
    ];

    const TOTAL_ACHIEVEMENTS = 167;

    /**
     * @var WeaponsMastery
     */
    private $weaponsMastery;

    /**
     * ScoreBoardController constructor.
     */
    public function __construct()
    {

        $this->weaponsMastery = new WeaponsMastery();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {

        return view('scoreboard')->with(
            [
                'teamName' => env('APP_NAME'),
                'playersData' => $this->getPlayersData()->sortByDesc('score'),
                'subtitle' => $this->getQuote()
            ]
        );
    }

    public function debug()
    {

        dd($this->getPlayersData()->sortByDesc('score'));
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getPlayersData(): \Illuminate\Support\Collection
    {
        $players = $this->getPlayers();
        $steamIds = $this->getSteamIds($players->toArray());
        $playersProfile = $this->getPlayersProfiles($steamIds);

        $playersData = $this->createPlayersDataArray($playersProfile);
        $playersData = $this->addStatsToPlayerData($playersData);
        $playersData = $this->calculateScore($playersData);
        $playersData = $this->weaponsMastery->getWeaponMasters($playersData);

        return collect($playersData);
    }

    /**
     * @return Player[]|Collection
     */
    private function getPlayers()
    {
        return Player::all();
    }

    /**
     * @param array $players
     * @return array
     */
    private function getSteamIds(array $players): array
    {

        return array_map(
            function ($player) {
                return $player['steamId'];
            },
            $players
        );
    }

    /**
     * @param string|array $steamIds
     * @return array
     */
    private function getPlayersProfiles($steamIds): array
    {

        $steamApi = new Steam;
        $users = $steamApi->user($steamIds);
        return $users->GetPlayerSummaries();
    }

    /**
     * @param string $steamId
     * @return object
     */
    private function getPlayerStats(string $steamId): object
    {

        $steamApi = new Steam();
        $stats = $steamApi->userStats($steamId);
        return $stats->GetUserStatsForGame(730, true);
    }

    /**
     * @param array $playersData
     * @return array
     */
    private function addStatsToPlayerData(array $playersData): array
    {

        foreach ($playersData as $steamId => $data) {
            $stats = $this->getPlayerStats($steamId);
            $sortedStats = [];
            foreach ($stats->stats as $stat) {
                $sortedStats[$stat->name] = $stat->value;
            }
            $playersData[$steamId]['stats'] = $sortedStats;
            $playersData[$steamId]['achievementsCounts'] = count($stats->achievements);
            $playersData[$steamId]['achievementsPercent'] = round((count($stats->achievements) / self::TOTAL_ACHIEVEMENTS) * 100, 2);
        }

        return $playersData;
    }

    /**
     * @param array $playersProfile
     * @return array
     */
    private function createPlayersDataArray(array $playersProfile): array
    {
        $playersData = [];
        foreach ($playersProfile as $profile) {
            $playersData[$profile->steamId] = ['profile' => $profile];
        }
        return $playersData;
    }

    /**
     * @param array $playersData
     * @return array
     */
    private function calculateScore(array $playersData): array
    {
        // (kills - deaths + headshots * 2 +(planted bombs + defused bombs + rescued hostage) + total_mvps) / total rounds

        $lastScore = [
            'score' => 0,
            'owner' => null
        ];
        foreach ($playersData as $steamId => $data) {
            $stats = $data['stats'];
            $score = round(
                ((($stats['total_kills'] - $stats['total_deaths']) / 100 + $stats['total_kills_headshot'] +
                    $stats['total_planted_bombs'] + $stats['total_defused_bombs'] + $stats['total_rescued_hostages']
                    + $stats['total_mvps']) / $stats['total_rounds_played']) ,
                2
            );
            $playersData[$steamId]['score'] = $score;

            if ($lastScore['score'] < $score) {
                $playersData[$steamId]['isLeader'] = true;
                if ($lastScore['owner']) {

                   $playersData[$lastScore['owner']]['isLeader'] = false;
                }
                $lastScore['score'] = $score;
                $lastScore['owner'] = $steamId;
            } else {
                $playersData[$steamId]['isLeader'] = false;
            }
        }

        return $playersData;
    }

    /**
     * @return string
     */
    private function getQuote(): string
    {

        return self::QUOTES[array_rand(self::QUOTES, 1)];
    }
}
