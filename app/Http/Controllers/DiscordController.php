<?php

namespace App\Http\Controllers;

use Discord\Discord;
use Discord\Exceptions\IntentException;
use Discord\Parts\Channel\Message;
use Discord\Parts\Embed\Embed;
use Discord\WebSockets\Event;
use Exception;
use Illuminate\Support\Facades\Http;
use React\Promise\ExtendedPromiseInterface;

class DiscordController extends Controller
{

    const COMMANDS = [
        'csScore' => 'getCsPlayersScore',
        'ChuckNorris' => 'getChuckNorrisJoke'
    ];

    /**
     * @var string
     */
    private $commandPrefix;
    /**
     * @var Discord
     */
    private $discord;

    /**
     * DiscordController constructor.
     * @throws IntentException
     */
    public function __construct()
    {
        $this->commandPrefix = env('COMMAND_PREFIX');
        $this->discord = new Discord(['token' => env('BOT_TOKEN')]);
        $this->discord->on(
            'ready',
            function () {
                $this->discord->on(
                    Event::MESSAGE_CREATE,
                    function (Message $message) {
                        $this->listenCommands($message);
                    }
                );
            }
        );
    }

    /**
     * @return Discord
     */
    public function getDiscordInstance(): Discord
    {
        return $this->discord;
    }

    /**
     * @param Message $message
     * @throws Exception
     */
    private function listenCommands(Message $message)
    {
        $commandPosition = strpos($message->content, $this->commandPrefix, 0);
        if ($commandPosition === 0 && !$message->author->bot) {
            foreach (self::COMMANDS as $command => $methodName)
            if(strtolower($message->content) === strtolower($this->commandPrefix) . strtolower($command)) {
                $this->$methodName($message);
            }
        }
    }

    /**
     * @param Message $message
     * @return ExtendedPromiseInterface
     * @throws Exception
     */
    private function getCsPlayersScore(Message $message): ExtendedPromiseInterface
    {
        $scoreBoardController = new ScoreBoardController();
        $playersData = $scoreBoardController->getPlayersData()->sortByDesc('score')->toArray();
        $scores = [];

        foreach ($playersData as $playerData) {
            $scores[$playerData['profile']->personaName] = $playerData['score'];
        }

        $embed = new Embed($this->discord);
        $embed->setTitle('ElPatrons CS:GO score');
        $embed->setImage(asset('images/csgo.jpg'));
        $embed->setURL('http://elpatrons.grindwise.ee');
        foreach ($scores as $name => $score) {
            $embed->addFieldValues($name, $score);
        }

        return $message->channel->sendMessage('',false, $embed);
    }

    /**
     * @param Message $message
     * @return ExtendedPromiseInterface
     * @throws Exception
     */
    private function getChuckNorrisJoke(Message $message): ExtendedPromiseInterface
    {
        $response = json_decode(Http::get('https://api.chucknorris.io/jokes/random')->body(), false);
        return $message->channel->sendMessage($response->value);
    }
}
