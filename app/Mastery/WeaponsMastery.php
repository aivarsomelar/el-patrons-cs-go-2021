<?php
namespace App\Mastery;

/**
 * Class WeaponsMastery
 * @package App\Mastery
 */
class WeaponsMastery
{

    /**
     * @var string
     */
    const PREFIX = 'total_kills_';

    /**
     * @var array
     */
    const WEAPONS = [
        'pistol' => ['glock', 'deagle', 'elite', 'fiveseven', 'tec9', 'p250', 'hkp2000'],
        'heavy' => ['xm1014', 'nova', 'negev', 'sawedoff', 'mag7', ],
        'smg' => ['mac10', 'ump45', 'p90', 'mp7', 'mp9', 'bizon'],
        'riffle' => ['ak47', 'famas', 'm4a1', 'galilar'],
        'optic' => ['awp', 'aug', 'g3sg1', 'sg556', 'scar20', 'ssg08']
    ];

    /**
     * @var array
     */
    private $lastMaster = [
        'pistol' => [
            'score' => 0,
            'steamId' => null
        ],
        'heavy' => [
            'score' => 0,
            'steamId' => null
        ],
        'smg' => [
            'score' => 0,
            'steamId' => null
        ],
        'riffle' => [
            'score' => 0,
            'steamId' => null
        ],
        'optic' => [
            'score' => 0,
            'steamId' => null
        ]
    ];

    /**
     * @var array
     */
    private $playersData;

    /**
     * @param array $playersData
     * @return array
     */
    public function getWeaponMasters(array $playersData): array
    {
        $this->playersData = $playersData;
        foreach ($this->playersData as $steamId => $data) {
            foreach (self::WEAPONS as $gunType => $names)
                $this->setWeaponMaster($steamId, $data, $gunType);
        }

        return $this->playersData;
    }

    /**
     * @param String $steamId
     * @param array $data
     * @param string $gunType
     * @return $this
     */
    private function setWeaponMaster(string $steamId,array $data, string $gunType): WeaponsMastery
    {

        $stats = $data['stats'];

        $score = 0;
        foreach (self::WEAPONS[$gunType] as $pistol) {
            $score += (array_key_exists(self::PREFIX . $pistol, $stats)) ? $stats[self::PREFIX . $pistol] : 0;
        }
        $score = $score / $stats['total_kills'];

        $this->setPlayerScore($score, $steamId, $gunType);
        $this->setLeader($score, $steamId, $gunType);

        return $this;
    }

    /**
     * @param float $score
     * @param string $steamId
     * @param string $gunType
     */
    private function setPlayerScore(float $score, string $steamId, string $gunType)
    {

        $this->playersData[$steamId]['mastery'][$gunType]['score'] = $score;
    }

    /**
     * @param float $score
     * @param string $steamId
     * @param string $gunType
     */
    private function setLeader(float $score, string $steamId, string $gunType)
    {

        if($this->lastMaster[$gunType]['score'] < $score) {
            $this->playersData[$steamId]['mastery'][$gunType]['isLeader'] = true;
            if($this->lastMaster[$gunType]['steamId']) {
                $lastMasterId = $this->lastMaster[$gunType]['steamId'];
                $this->playersData[$lastMasterId]['mastery'][$gunType]['isLeader'] = false;
            }
            $this->lastMaster[$gunType]['score'] = $score;
            $this->lastMaster[$gunType]['steamId'] = $steamId;
        } else {
            $this->playersData[$steamId]['mastery'][$gunType]['isLeader'] = false;
        }
    }
}
