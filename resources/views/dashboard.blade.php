<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-gray-700 text-gray-400 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6">
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <form method="POST" action="{{ route('addPlayer') }}" class="grid grid-cols-3 gap-4">
                        @csrf
                        <input type="text" placeholder="Nimi" name="name" aria-label="name"
                               class="m-2 rounded-md shadow-sm border-gray-700 focus:ring focus:ring-indigo-500 focus:ring-opacity-50 bg-gray-500 placeholder-gray-200">
                        <input type="text" placeholder="Steam Id" name="steamId" aria-label="steamId"
                               class="m-2 rounded-md shadow-sm border-gray-700 focus:ring focus:ring-indigo-500 focus:ring-opacity-50 bg-gray-500 placeholder-gray-200">
                        <button type="submit"
                               class="border border-green-500 bg-green-500 text-white rounded-md px-2 py-2 m-2 transition duration-500 ease select-none hover:bg-green-600 focus:outline-none focus:shadow-outline"
                        >Salvesta</button>
                    </form>
                </div>
                <hr class="border-gray-500">
                <div class="p-6">
                    @foreach($players as $player)
                    <div class="grid grid-cols-3 gap-4 mt-3 mb-3 items-center">
                        <div class="text-center">{{ $player->name }}</div>
                        <div class="text-center">{{ $player->steamId }}</div>
                        <div class="text-center">
                            <form method="POST" action="{{ route('deletePlayer') }}">
                                @csrf
                                <input type="hidden" name="id" value="{{$player->id}}">
                            <button type="submit"
                                class="border border-red-500 bg-red-500 text-white rounded-md px-2 py-2 m-2 transition duration-500 ease select-none hover:bg-red-600 focus:outline-none focus:shadow-outline"
                            >Kustuta</button>
                            </form>
                        </div>
                    </div>
                        <hr class="border-gray-500">
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
