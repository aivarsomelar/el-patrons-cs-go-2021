<!doctype html>
<html lang="en">
<head>
    <title>{{ $teamName }}</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!--  Fonts and icons  -->
    <!-- Fonts and icons -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

    <!-- Material Kit CSS -->
    <link href="{{ asset('material-kit/css/material-kit.css?v=2.0.7') }}" rel="stylesheet" />
    <style>
        body {
            /*background-color: #181714;*/
            background-image: linear-gradient(to right, #181714 , #221f18);
        }
        .subtitle {
            color: #454545;
        }
        .text-yellow {
            color: #ff9c00;
        }
        hr {
            border: 0;
            border-top: 1px solid #2c2c32;
        }
    </style>
</head>
<body><nav class="navbar navbar-color-on-scroll navbar-transparent fixed-top navbar-expand-lg bg-dark"  color-on-scroll="100">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand" href="https://demos.creative-tim.com/material-kit/index.html">
                {{$teamName}} </a>
        </div>
    </div>
</nav>


<div class="page-header header-filter" data-parallax="true" style="background-image: url({{asset('images/csgo.jpg')}})">
    <div class="container">
        <div class="row">
            <div class="col-md-8 ml-auto mr-auto">
                <div class="brand text-center">
                    <h1>{{$teamName}} CS:GO Stats</h1>
                    <h3 class="title text-center">{{$subtitle}}</h3>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="main main-raised" style="background: #222227">
    <div class="container">
        <div class="section text-center">
            <div class="row">
{{--                foreach start--}}
                @foreach($playersData as $kry => $data)
                    <div class="col-1 col-md-6 col-lg-2 mb-5">
                        <!-- Images -->
                        <div class="col-12">
                            <a href="{{$data['profile']->profileUrl}}">
                                <img src="{{$data['profile']->avatarFullUrl}}"
                                    alt="Circle Image"
                                    class="rounded-circle img-fluid"
                                />
                            </a>
                        </div>
                        {{-- User steam status --}}
                        <div class="col-12 mt-2">
                            {!! $data['profile']->personaState !!}
                        </div>
                        <!-- medal -->
                        <div class="col-12 pt-3 d-none d-lg-block" style="height: 100px">
                            @if($data['isLeader'])
                            <img src="{{asset('images/medal.png')}}"
                                 alt="Medal Image"
                                 class="rounded-circle img-fluid"
                            />
                            @endif
                        </div>
                        <!-- Names -->
                        <div class="col-12">
                            <strong class="text-yellow">{{$data['profile']->personaName}}</strong>
                        </div>
                        <!-- score -->
                        <div class="col-12 mt-3">
                            <p class="subtitle">Player Score</p>
                            <h3 class="text-white">
                                <strong>{{$data['score']}}</strong>
                            </h3>
                            <hr />
                        </div>
                        <!-- Total kills -->
                        <div class="col-12">
                            <p class="subtitle">Kills</p>
                            <h3 class="text-yellow">
                                <strong>{{$data['stats']['total_kills']}}</strong>
                            </h3>
                            <hr />
                        </div>
                        <!-- Deaths -->
                        <div class="col-12">
                            <p class="subtitle">Deaths</p>
                            <h3 class="text-yellow">
                                <strong>{{$data['stats']['total_deaths']}}</strong>
                            </h3>
                            <hr />
                        </div>
                        <!-- headshots -->
                        <div class="col-12">
                            <p class="subtitle">Headshots</p>
                            <h3 class="text-yellow">
                                <strong>{{$data['stats']['total_kills_headshot']}}</strong>
                            </h3>
                            <hr />
                        </div>
                        <!-- Total rounds -->
                        <div class="col-12">
                            <p class="subtitle">Total rounds</p>
                            <h3 class="text-yellow">
                                <strong>{{$data['stats']['total_rounds_played']}}</strong>
                            </h3>
                            <hr />
                        </div>
                        <!-- Planted and Defused -->
                        <div class="col-12">
                            <p class="subtitle">Planted</p>
                            <h3 class="text-yellow">
                                <strong>{{$data['stats']['total_planted_bombs']}}</strong>
                            </h3>
                            <hr />
                        </div>
                        <div class="col-12">
                            <p class="subtitle">Defused</p>
                            <h3 class="text-yellow">
                                <strong>{{$data['stats']['total_defused_bombs']}}</strong>
                            </h3>
                            <hr />
                        </div>
                        <div class="col-12">
                            <p class="subtitle">Rescued hostages</p>
                            <h3 class="text-yellow">
                                <strong>{{$data['stats']['total_rescued_hostages']}}</strong>
                            </h3>
                            <hr />
                        </div>
                        <div class="col-12">
                            <p class="subtitle">MVP</p>
                            <h3 class="text-yellow">
                                <strong>{{$data['stats']['total_mvps']}}</strong>
                            </h3>
                            <hr />
                        </div>
                        <div class="col-12">
                            <p class="subtitle">Achievements</p>
                            <h5 class="text-yellow">
                                <strong>{{$data['achievementsPercent']}}%</strong>
                            </h5>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: {{$data['achievementsPercent']}}%" aria-valuenow="{{$data['achievementsPercent']}}" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <hr />
                        </div>
                        {{-- Weapn mastery - keep it last because it does not have <hr/> at the end --}}
                        <div class="col-12">
                            <p class="subtitle">Weapon Mastery</p>
                            @foreach($data['mastery'] as $mastery => $masteryData)
                                @if($masteryData['isLeader'])
                                    <h5 class="text-yellow">
                                        <strong>{{$mastery}}</strong>
                                    </h5>
                                @endif
                            @endforeach
                        </div>
                    </div>
{{--                foreach end--}}
                @endforeach
            </div>
        </div>
    </div>
</div>

<footer class="footer footer-default text-white" >
    <div class="container">
        <nav class="float-left">
            <ul>
                <li>
                    <a href="/">
                        {{$teamName}}
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright float-right">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script>, made with <i class="material-icons">favorite</i> by
            <a href="https://bitbucket.org/aivarsomelar/" target="blank">Aivar Somelar</a> for a better web.
        </div>
    </div>
</footer>
<!--   Core JS Files   -->
<script src="{{asset('material-kit/js/core/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('material-kit/js/core/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('material-kit/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>
<script src="{{asset('material-kit/js/plugins/moment.min.js')}}"></script>
<!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
<script src="{{asset('material-kit/js/plugins/bootstrap-datetimepicker.js')}}" type="text/javascript"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{asset('material-kit/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
<!--  Google Maps Plugin  -->
<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
<script src="{{asset('material-kit/js/material-kit.js?v=2.0.7')}}" type="text/javascript"></script>
</body>
</html>
